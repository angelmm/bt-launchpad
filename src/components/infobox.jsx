import React from 'react';

const InfoBox = props => {
  return (
    <div className="info-box">
      <div className="info-box-image">
        <img alt={props.imageAlt} src={props.imageSrc} />
      </div>
      <div className="info-box-content">
        <p>{props.text} <a target="_blank" href={props.learnMore}>Learn More</a></p>
      </div>
    </div>
  )
}

export default InfoBox;
