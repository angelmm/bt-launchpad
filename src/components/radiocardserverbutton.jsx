import React from 'react';
import RadioButton from './radiobutton';

class RadioCardServerButton extends RadioButton {
  handleCardClick() {
    this.handleChange();
  }
  // Round to 2 decimals
  // TODO: Extract in a module/mixin
  round2(value) {
    return parseFloat(Math.round(value * 100) / 100).toFixed(2);
  }
  // Render the element
  render() {
    let checked;

    if (this.props.selected.value === this.props.value){
      checked = true;
    }

    return (
      <div className="radio-card-server-item" data-checked={checked}>
        <input type="radio" name={this.props.name} value={this.props.value} ref="radio" />
        <div className="radio-card-server-info" onClick={this.handleCardClick.bind(this)}>
          <h2><strong>{this.props.item.name}</strong></h2>
          <p className="radio-card-server-specs">{this.props.item.specs}</p>
          <p><strong>(${this.round2(this.props.item.hourCost * 24 * 30)} /mo)</strong>
            <span className="hourly-cost">${this.round2(this.props.item.hourCost)} /hr</span></p>
        </div>
      </div>
    )
  }
}

export default RadioCardServerButton;
