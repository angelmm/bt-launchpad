import React from 'react';

class RadioButton extends React.Component {
  handleChange() {
    this.props.onChange(
      this.props.name,
      this.refs.radio.value
    );
  }

  render() {
    let checked;

    if (this.props.selected.value === this.props.value){
      checked = 'checked';
    }

    return (
      <span className="radio-item">
        <input type="radio" name={this.props.name} value={this.props.value}
          onChange={this.handleChange.bind(this)} ref="radio" checked={checked} />
        {this.props.label}
      </span>
    )
  }
}

export default RadioButton;
