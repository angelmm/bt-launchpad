import React from 'react';
import Label from './label';
import RadioButton from './radiobutton';

class RadioGroup extends React.Component {
  getItem(el, i) {
    return <RadioButton key={i} value={el.value} name={this.props.name} label={el.label}
              onChange={this.props.onChange} selected={this.props.selected}/>;
  }
  render() {
    let items = [];

    for(let i = 0, l = this.props.collection.length; i < l; i++) {
      let item = this.props.collection[i];
      items.push(this.getItem(item, i));
    }

    return (
      <div className="input-group">
        <Label label={this.props.label} info={this.props.info} />
        <div className={"radio-group " + this.props.orientation}>
          { items }
        </div>
      </div>
    )
  }
}

export default RadioGroup;
