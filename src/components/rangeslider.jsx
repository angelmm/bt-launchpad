import React from 'react';
import Label from './label';
import Slider from 'react-rangeslider';

class RangeSlider extends React.Component {
  handleChange(value) {
    this.props.onUserInput(
      this.props.name,
      value
    );
  }

  render() {
    return (
      <div className="input-group range-slider">
        <Label label={this.props.label} info={this.props.info} />
        <Slider value={this.props.value} orientation="horizontal"
          onChange={this.handleChange.bind(this)} min={this.props.min}
          max={this.props.max} />
      </div>
    )
  }
}

export default RangeSlider;
