import React from 'react';
import RadioButton from './radiobutton';

class RadioServerButton extends RadioButton {
  // Round to 2 decimals
  // TODO: Extract in a module/mixin
  round2(value) {
    return parseFloat(Math.round(value * 100) / 100).toFixed(2);
  }
  // Render the element
  render() {
    let checked;

    if (this.props.selected.value === this.props.value){
      checked = 'checked';
    }

    return (
      <div className="radio-server-item">
        <input type="radio" name={this.props.name} value={this.props.value}
          onChange={this.handleChange.bind(this)} ref="radio" checked={checked} />
        <div className="radio-server-info">
          <p><strong>{this.props.item.name}</strong></p>
          <p><strong>(${this.round2(this.props.item.hourCost * 24 * 30)} /mo)</strong>
            <span className="hourly-cost">${this.round2(this.props.item.hourCost)} /hr</span></p>
        </div>
      </div>
    )
  }
}

export default RadioServerButton;
