import React from 'react';

const Label = props => {
  let css, infoSpan;
  // Set info class if this label include some help
  if (props.info !== undefined && props.info.length > 0) {
    css = 'info';
    infoSpan = <span data-info={props.info}>?</span>;
  }
  return (
    <label className={css}>
      {props.label} {infoSpan}
    </label>
  )
}

export default Label;
