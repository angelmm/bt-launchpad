import React from 'react';
import Label from './label';

class Select extends React.Component {
  handleChange() {
    this.props.onUserInput(
      this.props.name,
      this.refs.select.value
    );
  }
  render() {
    return (
      <div className="input-group">
        <Label label={this.props.label} info={this.props.info} />
        <select name={this.props.name} deafultValue={this.props.value}
          placeholder={this.props.placeholder} ref="select"
          onChange={this.handleChange.bind(this)} >
          {
            this.props.collection.map(function(el, i) {
              return <option key={i} value={el.value}>{el.name}</option>;
            })
          }
        </select>
      </div>
    )
  }
}

export default Select;
