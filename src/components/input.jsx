import React from 'react';
import Label from './label';

class Input extends React.Component {
  handleChange() {
    this.props.onUserInput(
      this.props.name,
      this.refs.input.value
    );
  }
  render() {
    let unit, unitClass;

    if (this.props.unit !== undefined){
      unitClass = "input-with-unit";
      unit = <span className="input-unit">{this.props.unit}</span>;
    }

    return (
      <div className={"input-group " + unitClass } data-name={this.props.name}>
        <Label label={this.props.label} info={this.props.info} />
        <input name={this.props.name} value={this.props.value}
          placeholder={this.props.placeholder} ref="input" type={this.props.type}
          onChange={this.handleChange.bind(this)} />
        { unit }
      </div>
    )
  }
}

Input.defaultProps = { type: 'text' };

export default Input;
