import React from 'react';
import Label from './label';
import RadioServerButton from './radioserverbutton';
import RadioCardServerButton from './radiocardserverbutton';
import RadioGroup from './radiogroup';

class RadioServerGroup extends RadioGroup {
  getItem(el, i) {
    if(this.props.type === 'card'){
      return <RadioCardServerButton key={i} item={el} value={el.value} name={this.props.name}
                onChange={this.props.onChange} selected={this.props.selected}/>;
    } else {
      return <RadioServerButton key={i} item={el} value={el.value} name={this.props.name}
                onChange={this.props.onChange} selected={this.props.selected}/>;
    }
  }
  render() {
    let items = [];

    for(let i = 0, l = this.props.collection.length; i < l; i++) {
      let item = this.props.collection[i];
      items.push(this.getItem(item, i));
    }

    return (
      <div className="input-group">
        <Label label={this.props.label} info={this.props.info} />
        <div className={this.props.type + " radio-group " + this.props.orientation}>
          { items }
        </div>
      </div>
    )
  }
}

export default RadioServerGroup;
