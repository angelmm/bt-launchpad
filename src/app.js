// Load styles
require('./stylesheets/main.sass')

// Imports
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, hashHistory } from 'react-router'
import AwsLaunchpad from './pages/awslaunchpad';
import AwsLaunchpadNew from './pages/awslaunchpadnew';

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={AwsLaunchpad}/>
    <Route path="/new" component={AwsLaunchpadNew}/>
  </Router>,
  document.getElementById('content'));
