// Stub data for this page
const StubData = {
  images: [
    {
      name: 'WordPress v4.5.1 (amiubuntu-x64-hvm-ebs)',
      placeholder: 'wordpress',
      value: 'worpress-v451-amiubuntu',
      info: {
        imageSrc: 'https://d33np9n32j53g7.cloudfront.net/assets/stacks/wordpress/img/wordpress-stack-110x117-95cc5cc975831baa456f27d7f19c342f.png',
        imageAlt: 'Wordpress logo',
        text: 'WordPress is a popular open-source blogging software. WordPress rose to popularity quickly because of it’s up-to-date development framework, extensive feature set, flexibility, rapid and multilingual publishing ability, multi-author support, and thriving community.',
        learnMore: 'https://bitnami.com/stack/wordpress'
      }
    },
    {
      name: 'Redmine v3.2.1 (amiubuntu-x64-hvm-ebs)',
      placeholder: 'redmine',
      value: 'redmine-v321-amiubuntu',
      info: {
        imageSrc: 'https://d33np9n32j53g7.cloudfront.net/assets/stacks/redmine/img/redmine-stack-110x117-71ba2f6b6d109d53921f07df50cc1060.png',
        imageAlt: 'Redmine logo',
        text: 'Redmine is a very popular Rails-based open source bug tracker to help you manage issues and tasks for multiple projects. It is extremely flexible, features a built-in wiki, time tracking, custom fields, role-based access, SCM integration (including git), and support for multiple projects',
        learnMore: 'https://bitnami.com/stack/redmine'
      }
    },
    {
      name: 'Gitlab v5.5.6 (amiubuntu-x64-hvm-ebs)',
      value: 'gitlab-v556-amiubuntu',
      placeholder: 'gitlab',
      info: {
        imageSrc: 'https://d33np9n32j53g7.cloudfront.net/assets/stacks/gitlab/img/gitlab-stack-110x117-eee248508c1e3b1d400938cff5c5e060.png',
        imageAlt: 'Gitlab logo',
        text: 'The GitLab open source edition, available from Bitnami, is a developer tool that allows users to collaborate on code, create new projects, and perform code reviews. When using GitLab, users can keep their code on their own servers, either in the cloud or on-premise.',
        learnMore: 'https://bitnami.com/stack/gitlab'
      }
    },
    {
      name: 'Ghost v7.8.7 (amiubuntu-x64-hvm-ebs)',
      value: 'ghost-v787-amiubuntu',
      placeholder: 'ghost',
      info: {
        imageSrc: 'https://d33np9n32j53g7.cloudfront.net/assets/stacks/ghost/img/ghost-stack-110x117-9dc967b07e73ec17ecd950e16f2f366d.png',
        imageAlt: 'Ghost',
        text: 'If you find blogging software too complex, you might be a good candidate to try Ghost. This open source project was started to make blogging fun again, put content front-and-center, and make it easy to build beautiful blogs that work on every device.',
        learnMore: 'https://bitnami.com/stack/ghost'
      }
    }
  ],
  cloudAccounts: [
    {
      name: '1 hour demo',
      value: 'demo1'
    },
    {
      name: '1 day demo',
      value: 'demo24'
    }
  ],
  diskTypes: [
    {
      label: 'General Purpose (SSD)',
      value: 'ssd',
    },
    {
      label: 'Magnetic',
      value: 'magnetic'
    }
  ],
  regions: [
    {
      name: 'US West (California)',
      value: 'us-west-1'
    },
    {
      name: 'Europe Central (Paris)',
      value: 'eu-central-1'
    }
  ],
  servers: [
    {
      name: 't2.nano',
      value: 0,
      specs: '1 CPU - 512MB RAM',
      hourCost: 0.009
    },
    {
      name: 't2.micro',
      value: 1,
      specs: '1 CPU - 1GB RAM',
      hourCost: 0.017
    },
    {
      name: 't2.small',
      value: 2,
      specs: '2 CPU - 1GB RAM',
      hourCost: 0.034
    },
    {
      name: 'm3.medium',
      value: 3,
      specs: '4 CPU - 2GB RAM',
      hourCost: 0.077
    }
  ]
}

export default StubData;
