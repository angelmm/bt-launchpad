import React from 'react';
import { Router, Route, Link, hashHistory } from 'react-router'
import AwsLaunchpad from './awslaunchpad';
import Title from '../components/title';
import Input from '../components/input';
import Select from '../components/select';
import InfoBox from '../components/infobox';
import RadioGroup from '../components/radiogroup';
import RadioServerGroup from '../components/radioservergroup';
import RangeSlider from '../components/rangeslider';
// Test data
import StubData from '../stores/stubdata';

class AwsLaunchpadNew extends AwsLaunchpad {
  // Layout
  render() {
    return (
      <article className="launchpad aws-launchpad">
        <Title title="New Virtual Machine"/>
        <p className="other-version">Try the <Link to="/">classic version</Link>.</p>
        <div className="pure-g">
          <div className="pure-u-md-1-2 pure-e">
            <Input label="name" placeholder={this.state.namePlaceholder}
              name="name" value={this.state.name} ref="name"
              onUserInput={this.onChangeProperty.bind(this)} />
            <Select label="Cloud account" info="Cloud account"
              name="cloudAccount" value={this.state.cloudAccount} ref="cloudAccount"
              collection={this.cloudAccounts}
              onUserInput={this.onChangeProperty.bind(this)} />
            <Select label="Region" info="Region info"
              name="region" value={this.state.region} ref="region"
              collection={this.regions}
              onUserInput={this.onChangeProperty.bind(this)} />
          </div>
          <div className="pure-u-md-1-2 pure-e">
            <Select label="image" info="Image info"
              name="image" value={this.state.image} ref="image"
              collection={this.images}
              onUserInput={this.onChangeImage.bind(this)} />
            <InfoBox imageSrc={this.state.image.info.imageSrc}
              imageAlt={this.state.image.info.imageAlt}
              text={this.state.image.info.text}
              learnMore={this.state.image.info.learnMore} />
          </div>
        </div>
        <div className="pure-g">
          <div className="pure-u-md-2-5 pure-e">
            <RadioGroup collection={this.diskTypes} orientation="horizontal"
              label="Disk type" info="Disk info" name="diskType" ref="diskType"
              onChange={this.onChangeDiskType.bind(this)} selected={this.state.diskType} />
          </div>
          <div className="pure-u-md-3-5 pure-e">
            <div className="pure-g">
              <div className="pure-u-md-16-24 pure-e rtop-margin">
                <RangeSlider label="Disk Size" info="Disk size info" name="diskSize"
                  ref="diskSizeSlider" onUserInput={this.onChangeProperty.bind(this)}
                  value={this.state.diskSize} />
                <div className="disk-prize">
                  ${this.round2(this.state.diskSize * this.state.diskPrize)} /mo
                </div>
              </div>
              <div className="pure-u-md-8-24 pure-e rtop-margin">
                <Input name="diskSize" ref="diskSizeInput"
                  onUserInput={this.onChangeProperty.bind(this)}
                  value={this.state.diskSize} unit="GB"/>
              </div>
            </div>
          </div>
        </div>
        <div class="pure-g">
          <div className="server-size-selector">
            <RadioServerGroup collection={this.servers} orientation="horizontal"
              label="Server size" info="Server size" name="serverSize" ref="serverSizeRadio" type="card"
              onChange={this.onChangeServerSize.bind(this)} selected={this.state.serverSize} />
          </div>
        </div>
        <div className="actions">
          <p className="monthly-cost">
            Estimated Monthly cost: <strong>${this.getMonthlyCost()} /mo</strong>
          </p>
          <div>
            <button className="btn btn-secondary">Cancel</button>
            <button className="btn btn-primary">Create</button>
          </div>
        </div>
      </article>
    );
  }
}

export default AwsLaunchpadNew;
