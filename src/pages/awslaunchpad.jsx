import React from 'react';
import { Router, Route, Link, hashHistory } from 'react-router'
import Title from '../components/title';
import Input from '../components/input';
import Select from '../components/select';
import InfoBox from '../components/infobox';
import RadioGroup from '../components/radiogroup';
import RadioServerGroup from '../components/radioservergroup';
import RangeSlider from '../components/rangeslider';
// Test data
import StubData from '../stores/stubdata';

class AwsLaunchpad extends React.Component {
  constructor() {
    super();

    // Stub data for images
    this.images = StubData.images;
    // Stub data for accounts
    this.cloudAccounts = StubData.cloudAccounts;
    // disk types
    this.diskTypes = StubData.diskTypes;
    // Regions
    this.regions = StubData.regions;
    // Servers
    this.servers = StubData.servers;

    this.state = {
      name: '',
      image: this.images[0],
      cloudAccount: this.cloudAccounts[0].value,
      region: this.regions[0].value,
      diskType: this.diskTypes[0],
      diskSize: 10,
      diskPrize: 0.12,
      serverSize: this.servers[0],
      namePlaceholder: this.getNamePlaceholder(this.images[0])
    }
  }
  // Update the monthly cost based on current state
  getMonthlyCost() {
    return this.round2(
      this.state.diskPrize * this.state.diskSize + this.state.serverSize.hourCost * 24 * 30
    )
  }
  // Update the property of the object
  onChangeProperty(name, value){
    this.setState({ [name]: value });
  }
  // Update the image
  onChangeImage(_, value){
    //this.setState({ image: value });
    let image = this.images.filter(im => im.value === value);
    this.setState({ image: image[0],
                    namePlaceholder: this.getNamePlaceholder(image[0]) });
  }
  // Update the diskType
  onChangeDiskType(_, value){
    //this.setState({ image: value });
    let diskType = this.diskTypes.filter(disk => disk.value === value);
    this.setState({ diskType: diskType[0] });
  }
  // Update server size
  onChangeServerSize(_, value){
    value = parseInt(value);
    let serverSize = this.servers.filter(server => server.value === value);
    this.setState({ serverSize: serverSize[0] });
  }
  // Return a name for a placeholder based on given image
  getNamePlaceholder(image) {
    return 'my-' + image.placeholder + '-server';
  }
  // Round to 2 decimals
  round2(value) {
    return parseFloat(Math.round(value * 100) / 100).toFixed(2);
  }
  // Layout
  render() {
    return (
      <article className="launchpad aws-launchpad">
        <Title title="New Virtual Machine"/>
        <p className="other-version">Try the <Link to="/new">new version</Link>.</p>
        <div className="pure-g">
          <div className="pure-u-md-1-3 pure-e">
            <Input label="name" placeholder={this.state.namePlaceholder}
              name="name" value={this.state.name} ref="name"
              onUserInput={this.onChangeProperty.bind(this)} />
            <Select label="Cloud account" info="Cloud account"
              name="cloudAccount" value={this.state.cloudAccount} ref="cloudAccount"
              collection={this.cloudAccounts}
              onUserInput={this.onChangeProperty.bind(this)} />
            <RadioGroup collection={this.diskTypes} orientation="horizontal"
              label="Disk type" info="Disk info" name="diskType" ref="diskType"
              onChange={this.onChangeDiskType.bind(this)} selected={this.state.diskType} />
            <div className="pure-g">
              <div className="pure-u-md-16-24 pure-e">
                <RangeSlider label="Disk Size" info="Disk size info" name="diskSize"
                  ref="diskSizeSlider" onUserInput={this.onChangeProperty.bind(this)}
                  value={this.state.diskSize} />
                <div className="disk-prize">
                  ${this.round2(this.state.diskSize * this.state.diskPrize)} /mo
                </div>
              </div>
              <div className="pure-u-md-8-24 pure-e">
                <Input name="diskSize" ref="diskSizeInput"
                  onUserInput={this.onChangeProperty.bind(this)}
                  value={this.state.diskSize} unit="GB"/>
              </div>
            </div>
            <div className="server-size-selector">
              <RangeSlider label="Server Size" info="Server Size" name="serverSize"
                ref="serverSizeRange" onUserInput={this.onChangeServerSize.bind(this)}
                min={0} max={3} value={this.state.serverSize.value} />
              <RadioServerGroup collection={this.servers} orientation="vertical"
                label="" info="" name="serverSize" ref="serverSizeRadio"
                onChange={this.onChangeServerSize.bind(this)} selected={this.state.serverSize} />
            </div>
            <p className="monthly-cost">
              Estimated Monthly cost: <strong>${this.getMonthlyCost()} /mo</strong>
            </p>
            <div>
              <button className="btn btn-secondary">Cancel</button>
              <button className="btn btn-primary">Create</button>
            </div>
          </div>
          <div className="pure-u-md-2-3 pure-e">
            <Select label="image" info="Image info"
              name="image" value={this.state.image} ref="image"
              collection={this.images}
              onUserInput={this.onChangeImage.bind(this)} />
            <InfoBox imageSrc={this.state.image.info.imageSrc}
              imageAlt={this.state.image.info.imageAlt}
              text={this.state.image.info.text}
              learnMore={this.state.image.info.learnMore} />
            <Select label="Region" info="Region info"
              name="region" value={this.state.region} ref="region"
              collection={this.regions}
              onUserInput={this.onChangeProperty.bind(this)} />
            <div className="region-image">
              <img src="/static/images/worldmap.png" />
            </div>
          </div>
        </div>
      </article>
    );
  }
}

export default AwsLaunchpad;
