# bt-launchpad

This application has been developed with `nodejs v4.2.1` and `npm 3.8.7`. To run it, you need to clone the repository and install the dependencies:

```bash
npm install
```

All the source code is in `src` folder:

* `components`: include the source code of all ReactJS components.
* `pages`: containers of ReactJS components. They are components too.
* `static`: static assets like images.
* `stylesheets`: page styles.
* `app.js`: entrypoint of the application.
* `index.html`: base template to build HTML file.

This application uses `webpack` to build assets and serve them in development mode (see next sections).

Production code is available at [https://bt-launchpad.herokuapp.com/](https://bt-launchpad.herokuapp.com/). The first load is usually slow because I'm using a free dyno server.

## New prototype

I've created a prototype of the launchpad based on the similarity of the different fields. This prototype reuse all components of `AWSLaunchpad` and include a new one called `RadioCardServerButton`.

To see the prototype click on *Try new version* link or visit [https://bt-launchpad.herokuapp.com/#/new](https://bt-launchpad.herokuapp.com/#/new)

## Run the server

### Development

Use the following command to run a development server and open your browser in [http://localhost:8080](http://localhost:8080).

```bash
npm run dev
```

This command will build all source code (JSX and SASS) from `src` folder and serve it. Webpack watches changes in files and reloads the page automatically.

### Production

To build assets and minify them use `npm run build`. This command will put output files into `dist` folder.

The project includes a simple `expressjs` server to serve files from `dist` folder. To run it, type `node server.js` in your console and visit [http://localhost:3000](http://localhost:3000). This code is available at [https://bt-launchpad.herokuapp.com/](https://bt-launchpad.herokuapp.com/).

You can execute both commands running `npm start`.
